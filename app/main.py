
from routers import sentiment

from fastapi import FastAPI

app = FastAPI()

app.include_router(sentiment.router, prefix="/sentiment") 

