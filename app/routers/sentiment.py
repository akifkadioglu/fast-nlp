from fastapi import APIRouter
from typing import Union
from polyglot.text import Text
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression

from pydantic import BaseModel



class Item(BaseModel):
    text: str


router = APIRouter()


texts = ["Bu ürün harika!", "Bu ürün berbat.", "Bu ürün fena değil."]
labels = ["olumlu", "olumsuz", "nötr"]
model = LogisticRegression()
vectorizer = TfidfVectorizer()

@router.post("/analysis/")
async def analysis(item:Item):

    new_X = vectorizer.transform([item.text])
    prediction = model.predict(new_X)
    return prediction[0]

@router.get("/train/")
async def train_model():
    X = vectorizer.fit_transform(texts)
    model.fit(X, labels)
    return "Trained"
