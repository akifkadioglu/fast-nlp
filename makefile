start:
	poetry run uvicorn --app-dir=app main:app --reload --workers 1 --port 8000